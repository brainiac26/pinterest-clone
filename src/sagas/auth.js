import axios from 'axios';
import { takeLatest, call, put } from 'redux-saga/effects';
import { GET_USER, LOGOUT } from '../constants/auth';
import { getUserSuccess, getUserFailure, logout } from '../actions/auth';
import config from '../config';

function doGetUser() {
    return axios.get(`${config.baseUrl}/auth`);
}

function* getUser() {
    try {
        const res = yield call(doGetUser);
        yield put(getUserSuccess(res.data));
    } catch (err) {
        yield put(getUserFailure(err));
    }
}

export function* watchGetUser() {
    yield takeLatest(GET_USER, getUser);
}

function doLogout() {
    return axios.get(`${config.baseUrl}/logout`);
}

function* logoutUser() {
    yield call(doLogout);
}

export function* watchLogoutUser() {
    yield takeLatest(LOGOUT, logoutUser);
}



