import { all } from 'redux-saga/effects';
import { watchFetchCards, watchAddCard, watchDeleteCard } from './cards';
import { watchGetUser, watchLogoutUser } from './auth';
import { watchSetAuthor } from './author';

export default function* rootSaga() {
    yield all([
        watchFetchCards(),
        watchAddCard(),
        watchDeleteCard(),
        watchGetUser(),
        watchLogoutUser(),
        watchSetAuthor()
    ]);
}

