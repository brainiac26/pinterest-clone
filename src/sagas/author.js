import { takeLatest, put } from 'redux-saga/effects';
import { SET_AUTHOR } from '../constants/author';
import { push } from 'react-router-redux';

function* setAuthor(action) {
    yield put(push(`/author/${action.author.name}`));
}

export function* watchSetAuthor() {
    yield takeLatest(SET_AUTHOR, setAuthor);
}



