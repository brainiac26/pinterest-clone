import axios from 'axios';
import { takeLatest,  call, put } from 'redux-saga/effects';
import { FETCH_CARDS, ADD_CARD, DELETE_CARD } from '../constants/cards';
import { fetchCardsSuccess, fetchCardsFailure, addCardSuccess, addCardFailure, deleteCardSuccess, deleteCardFailure }
    from '../actions/cards';
import { push } from 'react-router-redux';
import config from '../config';

function doDeleteCard(id) {
    return axios.delete(`${config.baseUrl}/cards/${id}`);
}

function* deleteCard(action) {
    try {
        const res = yield call(doDeleteCard, action.card._id);
        yield put(deleteCardSuccess(res.data));
    } catch (err) {
        yield put(deleteCardFailure(err));
    }
}

function* watchDeleteCard() {
    yield takeLatest(DELETE_CARD, deleteCard);
}

function doAddCard(card) {
    return axios.post(`${config.baseUrl}/cards`, card);
}

function* addCard(action) {
    try {
        const res = yield call(doAddCard, action.card);
        yield put(addCardSuccess((res.data)));
        yield put(push('/personal'));
    } catch (err) {
        yield put(addCardFailure(err));
    }
}

function* watchAddCard() {
    yield takeLatest(ADD_CARD, addCard);
}

function doFetchCards() {
    return axios.get(`${config.baseUrl}/cards`);
}

function* fetchCards() {
    try {
        const res = yield call(doFetchCards);
        yield put(fetchCardsSuccess(res.data));
    } catch (err) {
        yield put(fetchCardsFailure(err));
    }
}

function* watchFetchCards() {
    yield takeLatest(FETCH_CARDS, fetchCards);
}

export {
    watchFetchCards,
    watchAddCard,
    watchDeleteCard
}
