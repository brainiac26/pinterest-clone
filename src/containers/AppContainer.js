import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import * as cardsActions from '../actions/cards';
import * as authActions from '../actions/auth';
import Nav from '../components/Nav';

class App extends Component {
    componentDidMount() {
        this.props.fetchCards();
        this.props.getUser();
        this.props.redirectToHome();
    }
    render() {
        return (
            <div>
                <Nav user={this.props.user} logout={this.props.logout}/>

                {this.props.children}

            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    user: state.auth
});

const mapDispatchToProps = (dispatch) => ({
    fetchCards: () => {
        return dispatch(cardsActions.fetchCards());
    },
    getUser: () => {
        return dispatch(authActions.getUser());
    },
    logout: () => {
        return dispatch(authActions.logout());
    },
    redirectToHome: () => {
        return dispatch(push('/'));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(App);