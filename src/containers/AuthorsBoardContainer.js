import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as authorActionCreators from '../actions/author';
import Board from '../components/Board';

export class HomeContainer extends Component {
    render() {
        let { cards, user, author, setAuthor } = this.props;
        cards = cards.filter(card => {
            return card.createdBy._id === author._id;
        });
        return (
            <Board cards={cards} user={user} setAuthor={setAuthor} />
        )
    }
}

const mapStateToProps = (state) => ({
    cards: state.cards,
    user: state.auth,
    author: state.author
});

const mapDispatchToProps = (dispatch) => ({
    setAuthor: (author) => {
        return dispatch(authorActionCreators.setAuthor(author));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);

