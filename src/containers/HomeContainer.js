import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as authorActionCreators from '../actions/author';
import Board from '../components/Board';

export class HomeContainer extends Component {
    render() {
        let { cards, user} = this.props;
        if (user) {
            cards = cards.filter(card => {
                return card.createdBy._id !== user._id;
            })
        }
        return (
            <Board cards={cards} user={user} setAuthor={this.props.setAuthor}/>
        )
    }
}

const mapStateToProps = (state) => ({
    cards: state.cards,
    user: state.auth
});

const mapDispatchToProps = (dispatch) => ({
    setAuthor: (author) => {
        return dispatch(authorActionCreators.setAuthor(author));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);

