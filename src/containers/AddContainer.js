import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Button, Input  } from 'react-materialize';
import * as imageFormActionCreators from '../actions/imageForm';
import * as cardsActionCreators  from '../actions/cards';
import ReactFilestack from 'filestack-react';

class AddContainer extends Component {
    onSuccess(e) {
        this.props.onUploadSuccess(e.filesUploaded[0].url);
    }
    onError(e) {
        console.log(e);
    }
    handleChange(e) {
        this.props.seImageDesc(e.target.value);
    }
    handleSave() {
        const { description, imageUrl } = this.props.imageForm;
        this.props.saveCard({description, imageUrl});
    }
    render() {
        const apikey = 'AFXo5iBWQ3SLvUt8bABZtz';
        const options = {
            accept: 'image/*',
            maxFiles: 1
        };
        const { description, imageUrl, valid } = this.props.imageForm;
        return (
            <Row className="add-image">
                <Col className="s12 m6 offset-m3 l4 offset-l4">
                    <h4 className="text-center">Add image</h4>
                    <form>
                        <Input type="text" placeholder="Image description" value={description}
                               onChange={ this.handleChange.bind(this)}/>

                        { imageUrl && <Col className="img-wrapper"><img src={imageUrl}/></Col> }

                        <Row>
                            <Col s={12} m={imageUrl ? 6 : 12}>
                                <ReactFilestack
                                    apikey={apikey}
                                    options={options}
                                    onSuccess={this.onSuccess.bind(this)}
                                    onError={this.onError.bind(this)}
                                    render={({ onPick }) => (
                                        <Button waves='light' className="block" onClick={onPick} >
                                            {imageUrl ? 'Change image' : 'Add image'}
                                        </Button>
                                    )}
                                />
                            </Col>
                            <Col s={12} m={6}>
                                {imageUrl &&
                                <Button waves='light' className="block" disabled={!valid}
                                        onClick={this.handleSave.bind(this)}>
                                    Save
                                </Button>}
                            </Col>
                        </Row>
                    </form>
                </Col>
            </Row>
        )
    }
}

const mapStateToProps = (state) => ({
    imageForm: state.imageForm
});

const mapDispatchToProps = (dispatch) => ({
    onUploadSuccess: (url) => {
        return dispatch(imageFormActionCreators.uploadImage(url));
    },
    seImageDesc: (value) => {
        return dispatch(imageFormActionCreators.setImageDesc(value));
    },
    saveCard: (card) => {
        return dispatch(cardsActionCreators.addCard(card));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(AddContainer);