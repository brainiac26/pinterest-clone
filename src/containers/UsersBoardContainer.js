import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as authorActionCreators from '../actions/author';
import * as cardActionCreators from '../actions/cards';
import Board from '../components/Board';

export class HomeContainer extends Component {
    render() {
        let { cards, user } = this.props;
        cards = cards.filter(card => {
            return card.createdBy._id === user._id;
        });
        return (
            <Board cards={cards} user={user} setAuthor={this.props.setAuthor} deleteCard={this.props.deleteCard}/>
        )
    }
}

const mapStateToProps = (state) => ({
    cards: state.cards,
    user: state.auth,
    author: state.author
});

const mapDispatchToProps = (dispatch) => ({
    deleteCard: (card) => {
        return dispatch(cardActionCreators.deleteCard(card))
    },
    setAuthor: (author) => {
        return dispatch(authorActionCreators.setAuthor(author));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);

