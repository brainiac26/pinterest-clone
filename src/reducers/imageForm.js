import * as imageFormActions from '../constants/imageForm';
import * as cardsActions from '../constants/cards';

const initialState = {
    description: '',
    imageUrl: '',
    valid: false
};

export default (state = initialState, action) => {
    switch (action.type) {
        case imageFormActions.SET_IMAGE_DESC:
            return {...state, description: action.description, valid: !!(action.description && state.imageUrl)};
        case imageFormActions.UPLOAD_IMAGE:
            return {...state, imageUrl: action.imageUrl, valid: !!(state.description && action.imageUrl)};
        case cardsActions.ADD_CARD_SUCCESS:
            return initialState;
        default:
            return state;
    }
}