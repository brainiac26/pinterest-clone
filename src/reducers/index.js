import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import cards from './cards';
import imageForm from './imageForm';
import author from './author';
import auth from './auth';

export default combineReducers({cards, imageForm, author, auth, routing: routerReducer});