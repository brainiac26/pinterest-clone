import * as authorActions from '../constants/author';

const initialState = null;

export default (state = initialState, action) => {
    switch (action.type) {
        case authorActions.SET_AUTHOR:
            return action.author;
        default:
            return state;
    }

}