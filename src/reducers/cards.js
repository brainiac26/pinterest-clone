import * as cardsActions from '../constants/cards';

const initialState = [];

export default (state = initialState, action) => {
    switch (action.type) {
        case cardsActions.FETCH_CARDS_SUCCESS:
            return action.cards;
        case cardsActions.ADD_CARD_SUCCESS:
            return state.concat(action.card);
        case cardsActions.DELETE_CARD_SUCCESS:
            return state.filter(card => {
                return card._id !== action.card._id;
            });
        default:
            return state;
    }
};