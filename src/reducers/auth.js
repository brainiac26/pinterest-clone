import { GET_USER_SUCCESS, LOGOUT } from '../constants/auth';

const initialState = null;

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_USER_SUCCESS:
            return action.user;
        case LOGOUT:
            return initialState;
        default:
            return state;
    }
}
