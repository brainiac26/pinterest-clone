import { GET_USER, GET_USER_SUCCESS, GET_USER_FAILURE, LOGOUT} from '../constants/auth';

export const getUser = () => {
    return {
        type: GET_USER
    }
};

export const getUserSuccess = (user) => {
    return {
        type: GET_USER_SUCCESS,
        user
    }
};

export const getUserFailure = (err) => {
    return {
        type: GET_USER_FAILURE,
        err
    }
};

export const logout = () => {
    return {
        type: LOGOUT
    }
};