import { UPLOAD_IMAGE, SET_IMAGE_DESC} from '../constants/imageForm';

export const uploadImage = (imageUrl) => {
    return {
        type: UPLOAD_IMAGE,
        imageUrl
    }
};

export const setImageDesc = (description) => {
    return {
        type: SET_IMAGE_DESC,
        description
    }
};

// export const uploadImageSuccess = (url) => {
//     return {
//         type: filestackActions.UPLOAD_IMAGE_SUCCESS,
//         url
//     }
// };
//
// export const uploadImageFailure = (err) => {
//     return {
//         type: filestackActions.UPLOAD_IMAGE_FAILURE,
//         err
//     }
// };

// export const deleteImage = (url) => {
//     return {
//         type: filestackActions.DELETE_IMAGE,
//         url
//     }
// };
//
// export const deleteImageSuccess = () => {
//     return {
//         type: filestackActions.DELETE_IMAGE_SUCCESS
//     }
// };
//
// export const deleteImageFailure = (err) => {
//     return {
//         type: filestackActions.UPLOAD_IMAGE_FAILURE,
//         err
//     }
// };