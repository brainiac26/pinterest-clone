import { SET_AUTHOR} from '../constants/author';

export const setAuthor = (author) => {
    return {
        type: SET_AUTHOR,
        author
    }
};