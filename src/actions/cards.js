import { ADD_CARD, ADD_CARD_SUCCESS, ADD_CARD_FAILURE,
    DELETE_CARD, DELETE_CARD_SUCCESS, DELETE_CARD_FAILURE, FETCH_CARDS, FETCH_CARDS_SUCCESS, FETCH_CARDS_FAILURE }
    from '../constants/cards';

export const addCard = (card) => {
    return {
        type: ADD_CARD,
        card
    }
};

export const addCardSuccess = (card) => {
    return {
        type: ADD_CARD_SUCCESS,
        card
    }
};

export const addCardFailure = (err) => {
    return {
        type: ADD_CARD_FAILURE,
        err
    }
};

export const deleteCard = (card) => {
    return {
        type: DELETE_CARD,
        card
    }
};

export const deleteCardSuccess = (card) => {
    return {
        type: DELETE_CARD_SUCCESS,
        card
    }
};

export const deleteCardFailure = (err) => {
    return {
        type: DELETE_CARD_FAILURE,
        err
    }
};

export const fetchCards = () => {
    return {
        type: FETCH_CARDS
    }
};

export const fetchCardsSuccess = (cards) => {
    return {
        type: FETCH_CARDS_SUCCESS,
        cards
    }
};

export const fetchCardsFailure = (err) => {
    return {
        type: FETCH_CARDS_FAILURE,
        err
    }
};