import React from 'react';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import { Provider } from 'react-redux';
import { syncHistoryWithStore } from 'react-router-redux';
import { connectedRouterRedirect } from 'redux-auth-wrapper/history3/redirect'
import store from './store';
import App from './containers/AppContainer';
import HomeContainer from './containers/HomeContainer';
import AddContainer from './containers/AddContainer';
import AuthorsBoardContainer from './containers/AuthorsBoardContainer';
import UsersBoardContainer from './containers/UsersBoardContainer';

const history = syncHistoryWithStore(hashHistory, store);

const userIsAuthenticated = connectedRouterRedirect({
    redirectPath: '/',
    authenticatedSelector: state => state.auth !== null,
    wrapperDisplayName: 'UserIsAuthenticated'
});

const routes = (
    <Provider store={store}>
        <Router history={history}>
            <Route path="/" component={App}>
                <IndexRoute component={HomeContainer}/>
                <Route path="/add" component={userIsAuthenticated(AddContainer)}/>
                <Route path="/author/:username" component={AuthorsBoardContainer}/>
                <Route path="/personal" component={userIsAuthenticated(UsersBoardContainer)}/>
            </Route>
        </Router>
    </Provider>
);

export default routes;