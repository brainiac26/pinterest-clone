import React, { Component } from 'react';
import { connect } from 'react-redux';
import { hashHistory } from 'react-router';

const requiresAuth = (ComposedComponent) => {
    class Auth extends Component {
        render() {
            if (!this.props.user) {
                hashHistory.push('/');
            }
            return <ComposedComponent/>;
        }
    }

    const mapStateToProps = (state) => ({
        user: state.auth
    });

    const mapDispatchToProps = () => ({});

    return connect(mapStateToProps, mapDispatchToProps)(Auth);
};


export default requiresAuth;