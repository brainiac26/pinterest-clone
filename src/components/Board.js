import React from 'react';
import { Card, CardTitle, Button, Col, Row } from 'react-materialize';
import { Link } from 'react-router';
import Masonry from 'react-masonry-component';

var masonryOptions = {
    transitionDuration: 300
};

const Board = ({cards, user, deleteCard, setAuthor}) => {
    const cardElements = cards.map(card => {
        let actions;
        if (user && card.createdBy._id === user._id) {
            actions = (
                <Button waves="light" className="red" onClick={() => deleteCard(card)}>Delete</Button>
            )
        } else {
            actions = (
                <span key={1}>
                        By
                        <a className="username" onClick={() => setAuthor(card.createdBy)}>
                            {card.createdBy.name}
                        </a>
                    </span>
            )
        }
        return (
            <Col l={4} m={6} s={12} key={card._id}>
                <Card actions={[actions]}
                      header={<CardTitle image={card.imageUrl}></CardTitle>}>
                    {card.description}
                </Card>
            </Col>
        )
    });
    return (
        <div className="board-wrapper">
            {!cardElements.length && <h4 className="text-center">Nothing to show yet</h4>}
            <Row>
                <Masonry
                    className={'my-gallery-class'}
                    elementType={'ul'}
                    options={masonryOptions}
                    disableImagesLoaded={false}
                    updateOnEachImageLoad={false}
                >
                    {cardElements}
                </Masonry>
            </Row>

            {user && <Link to="/add">
                <Button floating
                        large
                        className='add red'
                        waves='light'
                        icon='add'/>
            </Link>}
        </div>
    )
};

export default Board;