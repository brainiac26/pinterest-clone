import React from 'react';
import { Navbar, NavItem, Icon, Dropdown } from 'react-materialize';

const Nav = ({user, logout}) => (
    <Navbar brand='Pikterest' right>
        <NavItem href="/"/>
        {!user && <NavItem href="/login">Sign up</NavItem>}
        {user && <NavItem>Hi {user.name}</NavItem>}
        {user &&
        <Dropdown trigger={
            <NavItem><Icon>more_vert</Icon></NavItem>
        }>
            <NavItem href="/#/personal">Your board </NavItem>
            <NavItem divider />
            <NavItem onClick={logout}>Logout</NavItem>
        </Dropdown>}
    </Navbar>
);

export default Nav;