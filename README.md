# Pinterest clone

Image board like pinterest. Part of [FCC](https://www.freecodecamp.org/challenges/build-a-pinterest-clone)

Client side was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

[Demo](https://pikterest.herokuapp.com/)

## Run

Install dependencies

`npm install`

Run client

`npm run client`

Build client

`npm run build`

Run server

`npm start`


