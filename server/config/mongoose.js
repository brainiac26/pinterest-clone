var mongoose = require('mongoose');
var config = require('./config.js');

module.exports = function() {
    var db = mongoose.connect(config.db, {
        useMongoClient: true,
    });

    require('../app/models/user.model.js');
    require('../app/models/card.model.js');

    return db;
};
