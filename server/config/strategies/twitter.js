var passport = require('passport'),
    TwitterStrategy = require('passport-twitter').Strategy,
    User = require('mongoose').model('User');
    config = require('../config');

module.exports = function() {
    passport.use(new TwitterStrategy({
        consumerKey: config.twitter.consumerKey,
        consumerSecret: config.twitter.consumerSecret,
        callbackURL: config.twitter.callbackURL
    }, function(accessToken, refreshToken, profile, done) {
        var searchQuery = {
            twitterId: profile.id
        };

        var update = {
            twitterId: profile.id,
            name: profile.displayName
        };

        var updateOptions = {
            upsert: true
        };

        User.findOneAndUpdate(searchQuery, update, updateOptions, function(err, user) {
            return done(err, user);
        })

    }));
};

