var passport = require('passport');
var User = require('mongoose').model('User');

module.exports = function() {

    passport.serializeUser(function(user, done) {
        return done(null, user.id);
    });

    passport.deserializeUser(function(userId, done) {
        User.findById(userId, function(err, user) {
            return done(err, user);
        })
    });

    require('./strategies/twitter')();

};

