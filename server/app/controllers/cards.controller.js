var mongoose = require('mongoose');
var Card = mongoose.model('Card');

exports.list = function(req, res, next) {
    Card.find({}).populate('createdBy').exec(function(err, cards) {
        if (err) return next(err);
        return res.send(cards);
    })
};

exports.add = function(req, res, next) {
    var card = new Card(req.body);
    card.createdBy = req.user;
    card.save(function(err) {
        if (err) return next(err);
        return res.send(card);
    })
};

exports.canDelete = function(req, res, next) {
    Card.findById(req.params.id, function(err, card) {
        if (err) return next(err);
        if (!card) return res.status(500).send({message: 'No card found'});
        if (card.createdBy != req.user.id) {
            return res.status(403).send({message: 'Forbidden'});
        }
        req.card = card;
        next();
    })
};

exports.delete = function(req, res, next) {
    const card = req.card;
    card.remove(function(err) {
        if (err) return next(err);
        res.send(card);
    });
};