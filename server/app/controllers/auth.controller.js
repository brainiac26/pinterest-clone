var passport = require('passport');

exports.twitterAuth = function(req, res, next) {
    return passport.authenticate('twitter')(req, res, next);
};

exports.twitterCallback = function(req, res, next) {
    return passport.authenticate('twitter', {successRedirect: '/', failureRedirect: '/'})(req, res, next);
};

exports.logout = function(req, res) {
    req.logout();
    res.send({success: true});
};

exports.checkAuth = function(req, res, next) {
    if (!req.isAuthenticated()) return res.status(401).send('Not authenticated');
    return next();
};

exports.getUser = function(req, res) {
    res.send(req.user);
};

