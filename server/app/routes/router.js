var authController = require('../controllers/auth.controller.js');
var cardsController = require('../controllers/cards.controller.js');
var errorHandler = require('../utils/errorHandler');

module.exports = function(app) {

    // auth

    app.get('/login', authController.twitterAuth);

    app.get('/callback', authController.twitterCallback);

    app.get('/logout', authController.logout);

    app.get('/auth', authController.getUser);

    // cards

    app.get('/cards', cardsController.list);

    app.post('/cards', authController.checkAuth, cardsController.add);

    app.delete('/cards/:id', authController.checkAuth, cardsController.canDelete, cardsController.delete);

    // handle errors

    app.use(errorHandler);

};