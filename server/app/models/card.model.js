var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CardSchema = new Schema({
    createdBy: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    description: {
        type: String,
        required: true
    },
    imageUrl: {
        type: String,
        required: true
    }
});

var Card = mongoose.model('Card', CardSchema);

module.exports = Card;